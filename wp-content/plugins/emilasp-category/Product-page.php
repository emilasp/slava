<?php


class SexProductStruct
{
    private $parentCategoryMap = [
        'male'   => 217,
        'famale' => 224,
    ];


    public function __construct()
    {
        echo '<h2>Реструктурируем категории</h2>';
    }


    public function restructCategory()
    {
        $products = $this->getProducts(1, 1);


        foreach ($products as $product) {
            //$product = get_post( $product[2], OBJECT );
            $categories = wp_get_object_terms($product[2], 'product_cat');
            if (count($categories) > 1) {
                $tt = 10;

                if ($fmId = $this->getFMcat($categories)) {
                    foreach ($categories as $cat) {
                        $this->checkAddCat($cat->term_id, $fmId);
                    }
                }
            }
            $tt = 10;
        }
    }

    private function checkAddCat($catId)
    {
        if (!in_array($catId, $this->parentCategoryMap)) {
            $parents = get_ancestors($catId, 'product_cat');

            foreach ($parents as $parent) {
                if(!in_array($parent->term_id, $this->parentCategoryMap)) {
                    
                }
            }
        }

    }

    private function getFMcat($categories)
    {
        foreach ($categories as $cat) {
            if ($cat->term_id == $this->parentCategoryMap['male']) {
                return $this->parentCategoryMap['male'];
            }
            if ($cat->term_id == $this->parentCategoryMap['famale']) {
                return $this->parentCategoryMap['famale'];
            }
        }

        return false;
    }


    private function getProducts($catName, $parent)
    {
        $full_product_list = [];
        $loop              = new WP_Query(['post_type' => ['product', 'product_variation'], 'posts_per_page' => - 1]);

        while ($loop->have_posts()) : $loop->the_post();
            $theid   = get_the_ID();
            $product = new WC_Product($theid);
            // its a variable product
            if (get_post_type() == 'product_variation') {
                $parent_id = wp_get_post_parent_id($theid);
                $sku       = get_post_meta($theid, '_sku', true);
                $thetitle  = get_the_title($parent_id);

                // ****** Some error checking for product database *******
                // check if variation sku is set
                if ($sku == '') {
                    if ($parent_id == 0) {
                        // Remove unexpected orphaned variations.. set to auto-draft
                        $false_post                = [];
                        $false_post['ID']          = $theid;
                        $false_post['post_status'] = 'auto-draft';
                        wp_update_post($false_post);
                        if (function_exists(add_to_debug)) {
                            add_to_debug('false post_type set to auto-draft. id=' . $theid);
                        }
                    } else {
                        // there's no sku for this variation > copy parent sku to variation sku
                        // & remove the parent sku so the parent check below triggers
                        $sku = get_post_meta($parent_id, '_sku', true);
                        if (function_exists(add_to_debug)) {
                            add_to_debug('empty sku id=' . $theid . 'parent=' . $parent_id . 'setting sku to ' . $sku);
                        }
                        update_post_meta($theid, '_sku', $sku);
                        update_post_meta($parent_id, '_sku', '');
                    }
                }
                // ****************** end error checking *****************

                // its a simple product
            } else {
                $sku      = get_post_meta($theid, '_sku', true);
                $thetitle = get_the_title();
            }
            // add product to array but don't add the parent of product variations
            if ( ! empty( $sku )) {
                $full_product_list[] = [$thetitle, $sku, $theid];
            }
        endwhile;
        wp_reset_query();
        // sort into alphabetical order, by title
        sort($full_product_list);

        return $full_product_list;
    }

    private function addCat($catName, $parent)
    {

    }

    private function getProductCategories($parentId)
    {
        $args = [
            'hide_empty'   => false,
            'exclude'      => [],
            'exclude_tree' => [],
            'include'      => [],
            'fields'       => 'id=>name',
            'parent'       => $parentId,
        ];

        $myterms = get_terms(['product_cat'], $args);

        return $myterms;
    }


}

$structClass = new SexProductStruct();
$structClass->restructCategory();


echo "";

$categories = get_categories(['hide_empty' => false]);


//читаем значение текущее опции
$curCL = get_option('cats_list');
foreach ($categories as $category) {
    $checked = '';
    if (is_array($curCL) && in_array($category->cat_ID, $curCL)) {
        $checked = 'checked="checked"';
    }
    echo '<label><input ' . $checked . ' type="checkbox" name="cats_list[]" value="' . $category->cat_ID . '" /> ' . $category->name . '</label><br />';
}


