<?php
/*
Plugin Name: Emilasp Category
Plugin URI: http://wordpress.org
Description: Emilasp Category
Author: emilasp
Author URI: http://example.com
*/

// Вызов функции добавления административных меню
add_action('admin_menu', 'mt_add_pages');

// Сама функция, вызываемая выше
function mt_add_pages() {
    // Создание подменю в Options:
    //add_options_page('Test Options', 'Test Options', 8, 'testoptions', 'mt_options_page');

    // Создание подменю в Manage:
    //add_management_page('Test Manage', 'Test Manage', 8, 'testmanage', 'mt_manage_page');

    // Создание нового пункта меню верхнего уровня:
    add_menu_page('Emilasp', 'Emilasp', 8, __FILE__, 'mt_forward_page');

    // Создание подпункта в свежесозданном меню:
    add_submenu_page(__FILE__, 'Категории', 'Категории', 8, 'sub-page', 'mt_category_page');
    add_submenu_page(__FILE__, 'Товары', 'Товары', 8, 'sub-page', 'mt_forward_page');
    add_submenu_page(__FILE__, 'Forward reformat', 'Forward reformat', 8, 'sub-page', 'mt_forward_page');
}

/*// mt_options_page() выводит содержимое страницы меню Test Options
function mt_options_page() {
    echo "<h2>Test Options</h2>";
}

// mt_manage_page() выводит содержимое страницы меню Test Manage
function mt_manage_page() {
    echo "<h2>Test Manage</h2>";
}*/

// mt_toplevel_page() выводит содержимое страницы меню Test Toplevel
function mt_category_page() {
    include __DIR__ . DIRECTORY_SEPARATOR . 'Category-page.php';
}

function mt_product_page() {
    include __DIR__ . DIRECTORY_SEPARATOR . 'Product-page.php';
}

function mt_forward_page() {
    include __DIR__ . DIRECTORY_SEPARATOR . 'ForwardXml.php';
}
