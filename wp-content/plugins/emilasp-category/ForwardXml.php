<?php


class ForwardXml
{
    const FILE_XML = '/var/www/sites/slava/data/partners/forward/getcatalogxml.xml';
    const FILE_XML_TARGET = '/var/www/sites/slava/data/partners/forward/finish.xml';

    const SEX_MALE = 'Мужское';
    const SEX_FEMALE = 'Женское';
    const SEX_UNISEX = 'Унисекс';


    private $errors = [];


    public function __construct()
    {
        echo '<h2>Реструктурируем Xml</h2>';

        $this->restruct();


    }


    public function restruct()
    {
        echo 'Start';
        $data = $this->getXmlData();
        foreach ($data as $index => $items) {
            if ($index == 'products') {
                foreach ($items as $indexItem => $item) {
                    $item = $this->reformatItem($item);
                    if (!$item) {
                        //$data->removeChild($data[$index][$indexItem]);
                        unset($data[$index][$indexItem]);
                    } else {
                        $data[$index][$indexItem] = $item;
                    }
                }
            }
        }
        $isSave = $data->saveXML(self::FILE_XML_TARGET);
        echo 'End';
    }

    private function reformatItem(SimpleXMLElement $item)
    {
        $attr = $this->getAttributes($item);

        /*if ($this->isSale($attr['sale'])) {
            return false;
        }*/

        $sex              = $this->getSex($attr['sex']);
        $counts           = $attr['counts'];
        $category         = $attr['category'];
        $item['category'] = $this->getCategory($category, $sex);
        $item['counts']   = $this->getCounts($counts);
        $item['sex']      = $sex;

        $item = $this->setCategories($item, $this->getCategory($category, $sex));

        return $item;
    }


    private function getAttributes(SimpleXMLElement $item)
    {
        $arr = [
            'articul'      => '',
            'category'     => '',
            'sex'          => '',
            'year'         => '',
            'season'       => '',
            'title'        => '',
            'price'        => '',
            'sale'         => '',
            'novelty'      => '',
            'brand'        => '',
            'counts'       => '',
            'links'        => '',
            'links_suit'   => '',
            'color'        => '',
            'composition'  => '',
            'manufacturer' => '',
        ];
        foreach ($item->attributes() as $index => $value) {
            $arr[$index] = (string) $value;
        }

        return $arr;
    }

    /**
     * @return null|SimpleXMLElement
     * @throws Exception
     */
    private function getXmlData()
    {
        if (file_exists(self::FILE_XML)) {
            $xml = @simplexml_load_file(self::FILE_XML);

            return $xml;
        } else {
            throw new Exception('Not looad Xml file!');
        }
        return null;
    }

    private function isSale($sale)
    {

        return isset($sale) && $sale;
    }

    private function getSex($sexItem)
    {
        $sex = self::SEX_UNISEX;
        switch ($sexItem) {
            case 'male':
                $sex = self::SEX_MALE;
                break;
            case 'female':
                $sex = self::SEX_FEMALE;
                break;
        }

        return $sex;
    }

    private function getCounts($counts)
    {
        $arr = [];
        if ($counts) {
            $counts = json_decode($counts);
            foreach ($counts as $cnt => $val) {
                $arr[] = $cnt;
            }
            $counts = implode('|', $arr);
        }
        return $counts;
    }

    private function getCategory($category, $sex)
    {
        $categories = [
            "1"  => 'Куртки',  //1" name="Куртки"/>
            "2"  => 'Куртки>Тренировочные',  //1.0001" name="Куртки тренировочные"/>
            "3"  => 'Брюки',  //3" name="Брюки"/>
            "4"  => 'Куртки>Флисовые',  //1.0002" name="Куртки флисовые"/>
            "5"  => 'Брюки',  //3.0001" name="Брюки флисовые"/>
            "6"  => 'Футболки, Майки',  //6" name="Футболки, Майки"/>
            "8"  => 'Брюки',  //3.0002" name="Брюки ветрозащитные"/>
            "9"  => 'Рубашки Поло',  //9" name="Рубашки Поло"/>
            "11" => 'Брюки', //03.0003" name="Брюки утеплённые"/>
            "12" => 'Куртки>Утеплённые', //01.0005" name="Куртки утеплённые"/>
            "13" => 'Шорты, Бриджи', //13" name="Шорты, Бриджи"/>
            "15" => 'Костюмы', //15" name="Костюмы"/>
            "16" => 'Халаты', //16" name="Халаты"/>
            "18" => 'Жилеты', //18" name="Жилеты"/>
            "19" => 'Шапки, Бейсболки', //19" name="Шапки, Бейсболки"/>
            "20" => 'Обувь', //20" name="Спортивная обувь"/>
            "21" => 'Аксессуары', //21" name="Аксессуары"/>
            "22" => 'Термобельё', //22" name="Термобельё"/>
            "23" => 'Футболки>Короткий рукав', //06.0001" name="Футболки короткий рукав"/>
            "24" => 'Куртки>Спортивные (парадные)', //01.0007" name="Куртки спортивные (парадны
            "25" => 'Костюмы', //015.0001" name="Костюмы спортивные (Парад
            "26" => 'Шапки, шарфы', //019.0001" name="Шапки, шарфы"/>
            "27" => 'Брюки', //03.0004" name="Брюки спортивные (парадные
            "29" => 'Футболки>Длинный рукав', //06.0002" name="Футболки длинный рукав"/>
            "30" => 'Костюмы', //015.0002" name="Костюмы тренировочные"/>
            "31" => 'Шапки, Бейсболки>Бейсболки', //019.0002" name="Бейсболки"/>
            "32" => 'Брюки', //03.0005" name="Брюки тренировочные"/>
            "33" => 'Аксессуары>Полотенца', //021.0002" name="Полотенца"/>
            "34" => 'Майки', //06.0003" name="Майки"/>
            "35" => 'Костюмы', //015.0003" name="Костюмы флисовые"/>
            "36" => 'Аксессуары>Носки', //021.0003" name="Носки"/>
            "37" => 'Костюмы', //015.0004" name="Костюмы ветрозащитные"/>
            "38" => 'Аксессуары', //021.0004" name="Аксессуары"/>
            "39" => 'Костюмы', //015.0005" name="Костюмы утеплённые"/>
            "40" => 'Аксессуары>Перчатки', //021.0005" name="Перчатки"/>
            "44" => 'Куртки>Велюровые', //01.0008" name="Куртки велюровые"/>
            "45" => 'Брюки', //03.0006" name="Брюки велюровые"/>
            "46" => 'Куртки>Пуховики', //46" name="Пуховики"/>
            "49" => 'Обувь>Кроссовки', //020.0001" name="Кроссовки"/>
            "50" => 'Обувь>Для пляжа и бассейна', //020.0002" name="Обувь для пляжа и бассейн
            "51" => 'Толстовки', //51" name="Толстовки (Худи)"/>
            "53" => 'Аксессуары>Сумки, Рюкзаки', //53" name="Сумки, Рюкзаки"/>
            "56" => 'Куртки>Ветровки', //56" name="Ветровки (Куртки ветрозащитные)
            "58" => 'Костюмы', //015.0006" name="Костюмы велюровые"/>
            "60" => 'Мягкая игрушка', //059.0001" name="Мягкая игрушка"/>
            "61" => 'Брелоки', //059.0002" name="Брелоки"/>
            //"64" => 'Экипировка Олимпийской сборной', //64" name="Экипировка Олимпийской сборной
            "70" => 'Купальники', //068.0002" name="Купальники"/>
            "71" => 'Плавки', //068.0003" name="Плавки"/>
            "78" => 'Аксессуары>Очки и Маски', //068.0004" name="Очки и Маски"/>
            //"89" => 'Сочи 2014', //89" name="Коллекция Сочи 2014"/>
            //"90" => 'Динамо', //90" name="Динамо"/>
            //"91" => 'Фитнес, Гимнастика' //91" name="Фитнес, Гимнастика"/>
        ];

        $noUniSex = [
            "78", "61", "60", "53", "40", "38", "36", "33", "31", "26", "21", "19"
        ];

        if (in_array($category, $noUniSex)) {
            $sex = '';
        } else {
            $sex = $sex . '>';
        }

        if (isset( $categories[$category] )) {
            return $sex . $categories[$category];
        } else {
            $this->errors[$sex] = $category;
            return $sex . 'tosort';
        }
    }

    private function setCategories($item, $categories)
    {
        $categoryArr = explode('>', $categories);
        foreach ($categoryArr as $index => $cat) {
            $item['cat' . $index] = $cat;
        }
        return $item;
    }

}

$structClass = new ForwardXml();