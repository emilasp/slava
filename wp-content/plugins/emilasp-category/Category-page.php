<?php


class SexCategoryStruct
{
    private $male = [];
    private $female = [];

    private $parentCategoryMap = [
        'male'   => 217,
        'famale' => 224,
    ];


    public function __construct()
    {
        echo '<h2>Реструктурируем категории</h2>';
    }


    public function restructCategory()
    {
        /*Получаем категории верхнего уровня
        Проверяем что в категориях Мужское их нет - тогда создаём в мужской и женской
        Получаем категории следующего уровня

        */

        $this->male = $this->getProductCategories($this->parentCategoryMap['male'], true);
        $this->female = $this->getProductCategories($this->parentCategoryMap['famale'], true);

        $allCat = $this->addCategory(null, null, 'male', $this->parentCategoryMap['male']);
        $allCat = $this->addCategory(null, null, 'female', $this->parentCategoryMap['famale']);

        $tt = 10;
    }

    private function addCategory($parent, $name, $globCats, $parentCatGlob)
    {
        $cats = $this->getProductCategories($parent, false, [
            $this->parentCategoryMap['male'], $this->parentCategoryMap['famale']
        ]);

        if ($parent && !in_array($name, $this->{$globCats})) {
            //add cat $parentId - $name
            $id = $this->addTerm($name, $parentCatGlob);
            $this->{$globCats}[$id] = $name;
            //wp_set_object_terms( $parentCatGlob, $id, 'product_cat', true );
        }

        foreach ($cats as $idx => $cat) {
            if (!$parent) {
                $parent = $parentCatGlob;
            }
           $this->addCategory($id, $cat, $globCats, $parent);
        }
    }

    private function addTerm($name, $parentId)
    {
        /*global $wpdb;
        $term = term_exists( $name, 'product_cat', $parentId );
        if (!$term) {*/
            $ids = wp_set_object_terms( $parentId, $name, 'product_cat', true );
           /* $slug = sanitize_title($name);
            $query = "INSERT INTO slava.wp_terms (term_id, name, slug, term_group) VALUES (null, '$name', '$slug', 0);";
            $tt = $wpdb->query($query);
            $lastId = $wpdb->insert_id;*/
            return (isset($ids[0]) ? $ids[0] : null);
       /* }
        return isset($term['term_id']) ? $term['term_id'] : null;*/
    }



    private function getFMcat($categories)
    {
        foreach ($categories as $cat) {
            if ($cat->term_id == $this->parentCategoryMap['male']) {
                return $this->parentCategoryMap['male'];
            }
            if ($cat->term_id == $this->parentCategoryMap['famale']) {
                return $this->parentCategoryMap['famale'];
            }
        }

        return false;
    }


    private function addCat($catName, $parent)
    {

    }

    private function getProductCategories($parentId = null, $hierarchical = false, $exclude = [])
    {
        $args = [
            'hide_empty'   => false,
            'exclude'      => [],
            'exclude_tree' => $exclude,
            'include'      => [],
            'fields'       => 'id=>name',
            'hierarchical' => $hierarchical,
        ];

        if ($parentId) {
            $args['parent'] = $parentId;
        }

        $myterms = get_terms(['product_cat'], $args);

        return $myterms;
    }
}

$structClass = new SexCategoryStruct();
$structClass->restructCategory();


echo "";

$categories = get_categories(['hide_empty' => false]);


//читаем значение текущее опции
$curCL = get_option('cats_list');
foreach ($categories as $category) {
    $checked = '';
    if (is_array($curCL) && in_array($category->cat_ID, $curCL)) {
        $checked = 'checked="checked"';
    }
    echo '<label><input ' . $checked . ' type="checkbox" name="cats_list[]" value="' . $category->cat_ID . '" /> ' . $category->name . '</label><br />';
}


